﻿using System;
using System.IO;
using System.Text;
using HttpServer;

namespace labwork
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.OutputEncoding = Encoding.UTF8;
			Console.InputEncoding = Encoding.UTF8;

			string currentDir = Directory.GetCurrentDirectory();
			string site = currentDir + @"/site";
			MyHTTPServer server = new MyHTTPServer(site, 8000);
		}
	}
}