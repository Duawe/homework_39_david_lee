using System;

namespace labwork
{
	public class Cat
	{
		private static Random rnd = new Random();

		public string name;
		public int age;
		public int satiety;
		public int happiness;
		private bool sleep = false;

		public Cat(string name)
		{
			this.name = name;
			age = rnd.Next(1,5);
			satiety = rnd.Next(50,100);
			happiness = rnd.Next(50,100);
		}
		
		// Спящего кота нельзя кормить.
		// Игра со спящим котом разбудит его, но понизит уровень счастья на 5 единиц
		// Кормление кота повышает уровень сытости на 15 единиц и счастье на 5
		// Игра с котом повышает уровень счастья на 15 единиц и понижает уровень сытости на 10, но с шансом 1 к 3 приводит кота в ярость снижая уровень счастья до 0,
		// Если перекормить кота, то он будет опечален на 30 единиц.

		private void Play()
		{
			if (sleep == true)
			{
				sleep = false;
				happiness -= 5;
			}
			else
			{
				int rndAction = rnd.Next(1, 3);
			
				if (rndAction == 2)
				{
					happiness = 0;
					satiety -= 10;
				}
				else
				{
					happiness += 15;
					satiety -= 10;
				}
			}
		}

		private void Feed()
		{
			if (satiety > 150)
			{
				happiness -= 30;
			}
			
			if (sleep == true)
			{
				satiety += 0;
			}
			else
			{
				satiety += 15;
				happiness += 5;
			}
		}

		private void Sleep()
		{
			sleep = true;
		}
	}
}